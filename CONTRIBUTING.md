This project is built with and for, and hosted by, the Wikimedia movement.

Development and other interaction with the community is covered by the [Code of Conduct for MediaWiki technical spaces](https://www.mediawiki.org/wiki/Code_of_Conduct).

Please see [the Wikimedia Developer Portal](https://developer.wikimedia.org/) for guidance on contributing.
