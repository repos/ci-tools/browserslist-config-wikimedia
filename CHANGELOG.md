# Changelog

Versions and bullets are arranged contextually chronologically from latest to oldest.

## v0.7.0 / 2024-05-23
* Update basic supported browsers for 2024 (James D. Forrester)

## v0.6.1 / 2024-03-01
* Fix modern config (Sam Smith, Roan Kattouw)

## v0.6.0 / 2024-01-16
* package: Update URLs to GitLab/MediaWiki now we're not on GitHub (James D. Forrester)
* Add CONTRIBUTING.md linking to the CoC for Technical Spaces (James D. Forrester)
* build: Upgrade eslint-config-wikimedia from 0.24.0 to 0.26.0 (James D. Forrester)
* build: Upgrade qunit from 2.19.4 to 2.20.0 (James D. Forrester)
* CI: Switch from node16 to node18 (James D. Forrester)
* Update modern standards for 2024 (James D. Forrester)

## v0.5.1 / 2023-04-14
* modern-es6-only: Fix deprecated modern-es6-only list (Ed Sanders)

## v0.5.0 / 2023-04-12
* modern: Drop IE11, bump Safari to 11+, change "last 2 versions" to "last 3 years", and Firefox ESR (Ed Sanders)
* modern-es6-only: DEPRECATED. Use 'modern' instead which is now also limited to ES6. (Ed Sanders)

## v0.4.0 / 2022-04-20
* basic: Bump requirements for 2022 (James D. Forrester)
* modern: Bump Android to >= 5 (Ed Sanders)

## v0.3.0 / 2022-02-08
* build: Update dependencies to latest (Volker E)
* Add 'modern-es6-only' config to be used by Vue 3 apps (Michael Große)

## v0.2.0 / 2021-05-16
* Replace single config file with separate files (Volker E.)

## v0.1.0 / 2021-05-14
* Add initial browserslist and 'package.json' configuration (Volker E.)
